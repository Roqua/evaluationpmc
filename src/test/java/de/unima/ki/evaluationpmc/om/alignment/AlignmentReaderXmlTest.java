package de.unima.ki.evaluationpmc.om.alignment;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.unima.ki.evaluationpmc.exceptions.AlignmentException;


public class AlignmentReaderXmlTest {

	private AlignmentReader alignmentReader;
	private Alignment alignment;
	
	@Before
	public void init() {
		final String testAlignment = "src/test/resources/alignment-types-test.rdf";
		this.alignmentReader = new AlignmentReaderXml();
		try {
			this.alignment = this.alignmentReader.getAlignment(testAlignment);
		} catch (AlignmentException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void correspondenceTypeTest() {
		Correspondence c = this.alignment.get(0);
		assertEquals(Correspondence.TYPE_DIFFICULT, c.getType().get());
		c = this.alignment.get(1);
		assertEquals(Correspondence.TYPE_DIFFICULT_SIMILAR_VERB, c.getType().get());
		c = this.alignment.get(2);
		assertEquals(Correspondence.TYPE_ONE_WORD_SIMILAR, c.getType().get());
		c = this.alignment.get(3);
		assertEquals(Correspondence.TYPE_TRIVIAL, c.getType().get());
		c = this.alignment.get(4);
		assertEquals(Correspondence.TYPE_TRIVIAL_BASIC_NORM, c.getType().get());
		c = this.alignment.get(5);
		assertEquals(Correspondence.TYPE_TRIVIAL_EXTENDED_NORM, c.getType().get());
		c = this.alignment.get(6);
		assertEquals(Correspondence.TYPE_MISC, c.getType().get());
		c = this.alignment.get(7);
		assertEquals(false, c.getType().isPresent());
	}

}
