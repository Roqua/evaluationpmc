package de.unima.ki.evaluationpmc.renderer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import de.unima.ki.evaluationpmc.evaluation.TypeCharacteristic;
import de.unima.ki.evaluationpmc.exceptions.AlignmentException;
import de.unima.ki.evaluationpmc.exceptions.CorrespondenceException;
import de.unima.ki.evaluationpmc.om.alignment.Alignment;

public class HTMLTableRendererTest {

	private Renderer renderer;
	private Alignment mapping;
	private Alignment mapping2;
	private Alignment reference;
	private TypeCharacteristic characteristic;
	private TypeCharacteristic characteristic2;
	
	
	@Before
	public void init() {
		final String file = "src/test/resources/renderer-output/test-output-html.html";
		final String mappingPath = "src/test/resources/type-characteristic-test-matcher.rdf";
		final String mapping2Path = "src/test/resources/type-characteristic-test-matcher-2.rdf";
		final String referencePath = "src/test/resources/type-characteristic-test-reference.rdf";
		try {
			this.renderer = new HTMLTableRenderer(file);
			this.mapping = new Alignment(mappingPath);
			this.mapping2 = new Alignment(mapping2Path);
			this.reference = new Alignment(referencePath);
			this.characteristic = new TypeCharacteristic(mapping, reference);
			this.characteristic2 = new TypeCharacteristic(mapping2, reference);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (AlignmentException e) {
			e.printStackTrace();
		} catch (CorrespondenceException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void textFileRenderingMultipleCharacteristicsTest() {
		List<TypeCharacteristic> characteristics = new ArrayList<>();
		characteristics.add(this.characteristic);
		characteristics.add(this.characteristic2);
		try {
			this.renderer.render(characteristics, "Test");
			this.renderer.flush();
		} catch (IOException | CorrespondenceException e) {
			e.printStackTrace();
		}
	}
}
