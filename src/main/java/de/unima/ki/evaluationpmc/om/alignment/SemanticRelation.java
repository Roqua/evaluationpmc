package de.unima.ki.evaluationpmc.om.alignment;

public enum SemanticRelation {
	EQUIV, SUB, SUPER, DISJOINT, NA

}
