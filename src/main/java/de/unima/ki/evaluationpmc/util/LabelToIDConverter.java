package de.unima.ki.evaluationpmc.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.xml.sax.SAXException;

import de.unima.ki.evaluationpmc.pm.model.Activity;
import de.unima.ki.evaluationpmc.pm.model.IntermediateCatchEvent;
import de.unima.ki.evaluationpmc.pm.model.Model;
import de.unima.ki.evaluationpmc.pm.model.parser.BPMNParser;
import de.unima.ki.evaluationpmc.pm.model.parser.Parser;

public class LabelToIDConverter {

	private List<Model> models;
	private Map<String, String> activityMap;
	private int numNulls;
	private Workbook wb;
	private Sheet sheet;
	private int numRows;
	
	public LabelToIDConverter() {
		this.activityMap = new HashMap<String, String>();
		this.wb = new HSSFWorkbook();
		this.sheet = wb.createSheet("bpm-goldstandard-ids");
		this.numRows = 0;
	}
	
	public void initMap() {
		for(Model m : models) {
			for(Activity a : m.getActivities()) {
				this.activityMap.put(a.getId(), a.getLabel());
			}
			for(IntermediateCatchEvent catchEvent : m.getCatchEvents()) {
				this.activityMap.put(catchEvent.getId(), catchEvent.getLabel());
			}
		}
	}

	
	public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
		String[] modelIds = new String[]{
				"Cologne",
				"Frankfurt",
				"FU_Berlin",
				"Hohenheim",
				"IIS_Erlangen",
				"Muenster",
				"Potsdam",
				"TU_Munich",
				"Wuerzburg"
		};
		LabelToIDConverter conv = new LabelToIDConverter();
		Parser parser = new BPMNParser();
		List<Model> models = new ArrayList<>();
		for (int i = 0; i < modelIds.length; i++) {
			Model model = parser.parse("src/main/resources/data/dataset1/models/" + modelIds[i] + ".bpmn");
			model.setPrefix("http://" + modelIds[i] + "#");
			model.setId(modelIds[i]);
			models.add(model);
		}
		for(Activity a : models.get(0).getActivities()) {
			System.out.println(models.get(0).getPrefix() + a.toString());
		}
		conv.transformLabelsToIDs(models);
	}
	
	public void transformLabelsToIDs(List<Model> models) throws FileNotFoundException, IOException {
		final Reader in = new FileReader("src/main/resources/data/dataset1/goldstandard-bpmn-1.csv");
		final Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
		Model sourceModel = null;
		Model targetModel = null;
		this.numNulls = 0;
		int numLabelsTotal = 0;
		for(CSVRecord record : records) {
			String[] values = record.get(0).split(";");
			if(values[0].contains("Admission") && values[1].contains("Admission")) {
				System.out.println("--------------------------");
				this.writeToFile("", values[0], "", values[1]);
				for(Model m : models) {
					if(values[0].replace(" ", "_").contains(m.getId())) {
						sourceModel = m;
					} else if(values[1].replace(" ", "_").contains(m.getId())) {
						targetModel = m;
					}
				}
			} else {
				numLabelsTotal += 2;
				String sourceID = this.getLabelId(sourceModel, values[0]);
				String targetID = this.getLabelId(targetModel, values[1]);
				this.writeToFile(sourceModel.getPrefix(), sourceID, targetModel.getPrefix(), targetID);
				System.out.println(values[0] + " " + values[1]);
			}
		}
		System.out.println("Number of NULL occurrences : " + this.numNulls + "/" + numLabelsTotal);
		this.flushToFile();
	}
	//TODO headlines in csv removal
	public String getLabelId(Model m, String label) {
		label = label.toLowerCase();
		for(Activity a : m.getActivities()) {
			if(a.getLabel().toLowerCase().replace("§", "_").equals(label)) {
				return a.getId();
			}
		}
		for(IntermediateCatchEvent event : m.getCatchEvents()) {
			if(event.getLabel().toLowerCase().equals(label)) {
				return event.getId();
			}
		}
		this.numNulls++;
		return null;
	}
	
	public void writeToFile(String sourcePrefix, String sourceID, String targetPrefix, String targetID) {
		System.out.println(sourceID + " " + targetID);
		Row row = sheet.createRow(this.numRows);
		Cell cellA = row.createCell(0);
		cellA.setCellValue(sourcePrefix + sourceID);
		Cell cellB = row.createCell(1);
		cellB.setCellValue(targetPrefix + targetID);
		if(sourceID.contains("Admission") || targetID.contains("Admission")) {
			CellStyle style = wb.createCellStyle();
			style.setFillBackgroundColor(IndexedColors.RED.getIndex());
			cellA.setCellStyle(style);
			cellB.setCellStyle(style);
		}
		this.numRows++;
	}
	
	public void flushToFile() throws IOException {
		final FileOutputStream fOut = new FileOutputStream("src/main/resources/data/dataset1/test.xls");
		wb.write(fOut);
		fOut.close();
	}
}
