package de.unima.ki.evaluationpmc.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.ecs.storage.Array;
import org.xml.sax.SAXException;

import de.unima.ki.evaluationpmc.exceptions.AlignmentException;
import de.unima.ki.evaluationpmc.om.alignment.Alignment;
import de.unima.ki.evaluationpmc.om.alignment.Correspondence;
import de.unima.ki.evaluationpmc.pm.model.Activity;
import de.unima.ki.evaluationpmc.pm.model.IntermediateCatchEvent;
import de.unima.ki.evaluationpmc.pm.model.Model;
import de.unima.ki.evaluationpmc.pm.model.parser.BPMNParser;
import de.unima.ki.evaluationpmc.pm.model.parser.Parser;
/**
 * CSV header
 * label1;label2;x1;x2;x3;x4;x5;x6;x7;x8;x9;x10;x11;x12;x13;confidence
 *
 */
public class GSExcelToRDF {

	private List<Model> models;
	private Map<String, String> activityMap;
	private int numNulls;
	private int numConfs;
	private int numRows;
	
	public GSExcelToRDF() {
		this.activityMap = new HashMap<String, String>();
		this.numRows = 0;
		this.numConfs = 0;
	}
	
	public void initMap() {
		for(Model m : models) {
			for(Activity a : m.getActivities()) {
				this.activityMap.put(a.getId(), a.getLabel());
			}
			for(IntermediateCatchEvent catchEvent : m.getCatchEvents()) {
				this.activityMap.put(catchEvent.getId(), catchEvent.getLabel());
			}
		}
	}

	
	public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, AlignmentException {
		String[] modelIds = new String[]{
				"Cologne",
				"Frankfurt",
				"FU_Berlin",
				"Hohenheim",
				"IIS_Erlangen",
				"Muenster",
				"Potsdam",
				"TU_Munich",
				"Wuerzburg"
		};
		GSExcelToRDF conv = new GSExcelToRDF();
		Parser parser = new BPMNParser();
		List<Model> models = new ArrayList<>();
		for (int i = 0; i < modelIds.length; i++) {
			Model model = parser.parse("src/main/resources/data/dataset1/models/" + modelIds[i] + ".bpmn");
			model.setPrefix("http://" + modelIds[i] + "#");
			model.setId(modelIds[i]);
			models.add(model);
		}
		conv.transformLabelsToIDs(models);
	}
	
	public void transformLabelsToIDs(List<Model> models) throws FileNotFoundException, IOException, AlignmentException {
		final String OUTPUT_PATH = "src/main/resources/data/dataset1/goldstandard_experts/";
		final Reader in = new FileReader("src/main/resources/data/dataset1/GoldStandard_8experts.csv");
		final Iterable<CSVRecord> records = CSVFormat.EXCEL.
				withDelimiter(';').
				withHeader().
				parse(in);
		Model sourceModel = null;
		Model targetModel = null;
		Alignment alignment = null;
		this.numNulls = 0;
		int numLabelsTotal = 0;
		List<String> cache = new ArrayList<>();
		for(CSVRecord record : records) {
			String label1 = record.get("label1");
			String label2 = record.get("label2");
			String confidence = record.get("confidence");
			if(label1.contains("Admission") && label2.contains("Admission")) {
				if(!Objects.isNull(alignment)) {
					alignment.write(OUTPUT_PATH + sourceModel.getId() + "-" + targetModel.getId() + ".rdf");					
				}
				for(Model m : models) {
					if(label1.replace(" ", "_").contains(m.getId())) {
						sourceModel = m;
					} else if(label2.replace(" ", "_").contains(m.getId())) {
						targetModel = m;
					}
				}
				System.out.println("------------"+sourceModel.getId()+ "-" + targetModel.getId() +"------------");
				alignment = new Alignment();
				cache = new ArrayList<>();
			} else if(!label1.isEmpty() && !label2.isEmpty()){
				numLabelsTotal += 2;
				String sourceID = this.getLabelId(sourceModel, label1);
				String targetID = this.getLabelId(targetModel, label2);
				if(cache.contains(sourceID+targetID)) {
					System.out.println("--------Doppelt-------");
					System.out.println(label1 + " - " + label2);
					System.out.println(sourceID + " - " + targetID);
					System.out.println("----------------------");
				} else {
					cache.add(sourceID + targetID);
				}
//				System.out.println(label1 + "-" + label2);
				if(confidence.isEmpty()) {
					confidence = "0.0";
					this.numConfs++;
				}
				if(!Objects.isNull(sourceID) && !Objects.isNull(targetID)) {
					alignment.add(new Correspondence(sourceModel.getPrefix() + sourceID, 
							targetModel.getPrefix() + targetID, Double.valueOf(confidence.replace(',', '.'))));
				}
			}
		}
		alignment.write(OUTPUT_PATH + sourceModel.getId() + "-" + targetModel.getId() + ".rdf");
		System.out.println("Number of NULL occurrences : " + this.numNulls + "/" + numLabelsTotal);
		System.out.println("Nmber of missing confidences : " + this.numConfs);
	}
	
	public String getLabelId(Model m, String label) {
		label = getSanitizeLabel(label);
		for(Activity a : m.getActivities()) {
			String sanLabel = getSanitizeLabel(a.getLabel());
			if(sanLabel.equals(label)) {
				return a.getId();
			}
		}
		for(IntermediateCatchEvent event : m.getCatchEvents()) {
			if(event.getLabel().toLowerCase().equals(label)) {
				return event.getId();
			}
		}
		this.numNulls++;
		System.out.println(m.getId() + "  --  " + label);
		return null;
	}
	
	public static String getSanitizeLabel(String label) {
		label = label.trim();
		label = label.toLowerCase();
		label = label.replace("\n", "");
		label = label.replace("§", "");
		return label;
	}
	
}
