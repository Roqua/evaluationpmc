package de.unima.ki.evaluationpmc.pm.model.parser;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import de.unima.ki.evaluationpmc.pm.model.Model;

public interface Parser {
	
	public Model parse(String filepath) throws ParserConfigurationException, SAXException, IOException;

}
