package de.unima.ki.evaluationpmc.pm.model;

public class IntermediateCatchEvent {

	private String id;
	private String label;
	
	public IntermediateCatchEvent(String id, String label) {
		this.id = id;
		this.label = this.sanitize(label);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "IntermediateCatchEvent [id=" + id + ", label=" + label + "]";
	}
	
	//TODO: create general sanitization function for all model components
	private String sanitize(String s){
		return s.replaceAll("\n", " ").replaceAll("\\s+"," ");
	}
}
