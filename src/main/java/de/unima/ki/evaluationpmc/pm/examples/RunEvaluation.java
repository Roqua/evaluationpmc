package de.unima.ki.evaluationpmc.pm.examples;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import de.unima.ki.evaluationpmc.evaluation.TypeCharacteristic;
import de.unima.ki.evaluationpmc.exceptions.AlignmentException;
import de.unima.ki.evaluationpmc.exceptions.CorrespondenceException;
import de.unima.ki.evaluationpmc.om.alignment.Alignment;
import de.unima.ki.evaluationpmc.om.alignment.AlignmentReader;
import de.unima.ki.evaluationpmc.om.alignment.AlignmentReaderXml;
import de.unima.ki.evaluationpmc.om.alignment.Correspondence;
import de.unima.ki.evaluationpmc.renderer.HTMLTableRenderer;
import de.unima.ki.evaluationpmc.renderer.Renderer;

public class RunEvaluation {

	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, 
			IOException, AlignmentException, CorrespondenceException {
		/**
		 * Define multiple directories with matcher alginments.
		 * First entry of the array should be the directory of the reference alignment.
		 */
		final String OUTPUT_PATH = "src/main/resources/matcher-evaluation-summary.html";
		final String RESULTS_PATH = "src/main/resources/data/dataset1/results/";
		final String END_DIR = "/dataset1";
		final String[] dirs = new String[] {
				"src/main/resources/data/dataset1/goldstandard",
				RESULTS_PATH + "AML-PM" + END_DIR,
				RESULTS_PATH + "BPLangMatch" + END_DIR,
				RESULTS_PATH + "KnoMa-Proc" + END_DIR,
				RESULTS_PATH + "Know-Match-SSS" + END_DIR,
				RESULTS_PATH + "Match-SSS" + END_DIR,
				RESULTS_PATH + "OPBOT" + END_DIR,
				RESULTS_PATH + "pPalm-DS" + END_DIR,
				RESULTS_PATH + "RMM-NHCM" + END_DIR,
				RESULTS_PATH + "RMM-NLM" + END_DIR,
				RESULTS_PATH + "RMM-SMSL" + END_DIR,
				RESULTS_PATH + "TripleS" + END_DIR
		};
		final String[] mappingInfo = new String[] {
				"AML-PM",
				"BPLangMatch",
				"KnoMa-Proc",
				"Know-Match-SSS",
				"Match-SSS",
				"OPBOT",
				"pPalm-DS",
				"RMM-NHCM",
				"RMM-NLM",
				"RMM-SMSL",
				"TripleS"
		};
		final AlignmentReader alignReader = new AlignmentReaderXml();
		List<List<Alignment>> alignments = new ArrayList<>();
		for (int i = 0; i < dirs.length; i++) {
			List<Alignment> aligns = new ArrayList<>();
			Files.walk(Paths.get(dirs[i])).forEach(filePath -> {
				if(Files.isRegularFile(filePath)) {
					try {
						Alignment a = alignReader.getAlignment(filePath.toString());
						aligns.add(a);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
		});
			alignments.add(aligns);
		}
		List<List<TypeCharacteristic>> characteristics = new ArrayList<>();
		for (int i = 1; i < alignments.size(); i++) {
			List<Alignment> aligns = alignments.get(i);
			List<TypeCharacteristic> tcharacteristics = new ArrayList<>();
			for (int j = 0; j < aligns.size(); j++) {
				try {
				Alignment mapping = aligns.get(j);
				Alignment reference = alignments.get(0).get(j);
				TypeCharacteristic tc = new TypeCharacteristic(mapping, reference);
				tcharacteristics.add(tc);
				} catch (CorrespondenceException ex){
					System.err.println("exception");
				} catch(IndexOutOfBoundsException ex) {
					System.err.println("Number of reference and matcher "
							+ "alignments unequal : " + ex.getMessage());
				}
			}
			characteristics.add(tcharacteristics);
		}
		Renderer renderer = new HTMLTableRenderer(OUTPUT_PATH);
		for (int i = 0; i < characteristics.size(); i++) {
			renderer.render(characteristics.get(i), mappingInfo[i]);
		}
		renderer.flush();
	}

}
