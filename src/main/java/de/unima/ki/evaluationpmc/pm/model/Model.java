package de.unima.ki.evaluationpmc.pm.model;

import java.util.HashSet;
import java.util.Set;

public class Model {

	private String id;
	private String prefix;
	private Set<Activity> activities;
	private Set<Flow> flows;
	private Set<IntermediateCatchEvent> catchEvents;
	
	public Model() {
		this.activities = new HashSet<Activity>();
		this.catchEvents = new HashSet<IntermediateCatchEvent>();
	}
	
	public void addActivity(Activity activity) {
		this.activities.add(activity);
	}
	
	
	public void addFlow(Flow flow) {
		this.flows.add(flow);
	}
	
	public void addIntermediateCatchEvent(IntermediateCatchEvent interCatchEvent) {
		this.catchEvents.add(interCatchEvent);
	}

	public Set<IntermediateCatchEvent> getCatchEvents() {
		return catchEvents;
	}

	public void setCatchEvents(Set<IntermediateCatchEvent> catchEvents) {
		this.catchEvents = catchEvents;
	}

	public Set<Activity> getActivities() {
		return this.activities;
	}
	
	public String getLabelById(String id) {
		for (Activity a : this.activities) {
			if (a.getId().equals(id)) {
				return a.getLabel();
			}
		}
		return "?-" + id;
	}
	
	public void normalizeLabels() {
		for (Activity a : this.activities) {
			a.normalizeLabels();
		}
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
